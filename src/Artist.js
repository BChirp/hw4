import React, { Component } from 'react';
import {Link}  from 'react-router-dom';


class Artist extends Component {

  render() {
    let {artist, params} = this.props;
      return (
          <div>
              {
                artist.album.map((item, index) =>
                      {
                      let link = `${params.url}/${index}`;
                      return  (<Link key={index} className="artist" to={link}>{item.name}<br/></Link>)
                      })
              }
          </div>
      );
  }
}

export default Artist;
