import React, { Component } from 'react';
import {Link}  from 'react-router-dom';


class List extends Component {

  render() {
    let {myArtist} = this.props;
        return (
              <div>
                  {
                      myArtist.map((item, index) => {
                          let link = '/list/' + item.index;
                          return  <Link key={index} className="lists" to={link}>
                                  {item.name}<br/>
                                  </Link>
                          })
                  }
              </div>
          );
  }
}

export default List;
