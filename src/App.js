import React, { Component } from 'react';
import List from './List';
import Artist from './Artist';
import Album from './Album';
import Bad404 from './Bad404';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

const supportsHistory = 'pushState' in window.history;


class App extends Component{
  state = {
    myArtist: [],
  }

componentDidMount(){
  this.doTake();
}

  doTake = () => {
    fetch("http://www.json-generator.com/api/json/get/bGyYXYiCOG?indent=2")
    .then(function(response) {
    return response.json();
    }).then(data => {
      this.setState({
        myArtist: data
      });
    });
  };

  render(){
          return(
            <BrowserRouter basename="/" forceRefrech={!supportsHistory}>
                <Switch>
                    <Route path="/list" exact render={() => {
                      return <List myArtist={this.state.myArtist} />
                    }
                    } />
                    <Route path="/list/:id" exact render={({match}) => {
                      return <Artist  artist={this.state.myArtist[match.params.id]} params={match}/>
                    }
                    } />
                    <Route path="/list/:id/:album" exact render={({match}) => {
                      return <Album album={this.state.myArtist[match.params.id].album[match.params.album]} params={match}/>
                    }
                    } />
                    <Route component={Bad404} />
                </Switch>
            </BrowserRouter>
          )
  }
}



export default App;
