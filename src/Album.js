import React, { Component } from 'react';



class Album extends Component {

  render() {
    let {album, params} = this.props;
      return (
        <div>
              {
               album.compositions.map((item, index) => {
                      return (<div key={index} className='album'> {item.name}</div>)
                      })
              }
        </div>
    );
  }
}

export default Album;
